# MAINTAINERS

This documentation is meant for the maintainers of all-config.

```bash
# Ensure all tests pass.
npm run test
npm run coverage
npm run coverage-results

# TODO Bump the version number.

# TODO Publish the new version to npm.
```
