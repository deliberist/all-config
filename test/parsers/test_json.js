'use strict';

const expect = require('chai').expect;

const json_parser = require('../../lib/parsers/json');

describe('lib/parsers/json', function () {

    const test_data = Object.freeze({
        "root": {
            "one": 1,
            "two": 2.2
        },
        "anotherRoot": "3.3"
    });

    describe('Public API', () => {
        it('Has callable parse method', () => {
            expect(json_parser).to.respondTo('parse');
            expect(json_parser.parse).to.be.a('function');
        });

        it('Read JSON Data', () => {
            const json_data = JSON.stringify(test_data);
            const actual_data = json_parser.parse(json_data);
            expect(actual_data).to.deep.equal(test_data);
        });

        it('Undefined Data', () => {
            expect(json_parser.parse).to.throw('Cannot parse empty JSON data.');
        });

        it('Null Data', () => {
            expect(json_parser.parse.bind(null)).to.throw(
                'Cannot parse empty JSON data.');
        });

        it('Invalid Data', () => {
            const invalidData = 'balls trololololol';
            expect(json_parser.parse.bind(invalidData)).to.throw(
                'Cannot parse empty JSON data.');
        });
    });

});
