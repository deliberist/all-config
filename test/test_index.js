'use strict';

const expect = require('chai').expect;
const yacjs = require('../lib/all-config');
const root = require('../');

describe('index', function () {

    describe('Public API', function () {
        it('root === lib/yaconfig.js', function () {
            expect(root).to.deep.equal(yacjs);
        });
    });

});
