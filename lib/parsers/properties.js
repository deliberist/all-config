'use strict';

require('../utils/dependencies')('properties-reader');

const PropertiesReader = require('properties-reader');

/**
 * Parses properties file data from a string into a object.
 *
 * @param data The properties data to be converted into an object.
 * @returns {ReadonlyArray<any>} Frozen object from the supplied properties
 *    data.
 */
module.exports.parse = (data) => {
  if (typeof data === 'undefined' || data === null) {
    throw 'Cannot parse empty properties data.';
  }
  const properties = PropertiesReader();
  return Object.freeze(properties.read(data).path());
};
