'use strict';

require('../utils/dependencies')('ini');

const ini = require('ini');

/**
 * Parses a INI file data from a string into a object.
 *
 * @param data The INI data to be converted into an object.
 * @returns {ReadonlyArray<any>} Frozen object from the supplied INI data.
 */
module.exports.parse = (data) => {
  if (typeof data === 'undefined' || data === null) {
    throw 'Cannot parse empty INI data.';
  }
  return Object.freeze(ini.parse(data));
};
