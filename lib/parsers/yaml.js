'use strict';

require('../utils/dependencies')('yaml');

const yaml = require('yaml');

/**
 * Parses YAML file data from a string into a object.
 *
 * @param data The YAML data to be converted into an object.
 * @returns {ReadonlyArray<any>} Frozen object from the supplied YAML data.
 */
module.exports.parse = (data) => {
  if (typeof data === 'undefined' || data === null) {
    throw 'Cannot parse empty YAML data.';
  }
  return Object.freeze(yaml.parse(data));
};
