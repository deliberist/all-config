'use strict';

const {MissingFileFormatDependency} = require('./exceptions');

// TODO docs
// TODO unit test
module.exports = function () {
  console.log(arguments);
  for (let i in arguments){
    const dep = arguments[i];
    try {
      require(dep);
    } catch (e) {
      throw new MissingFileFormatDependency(dep);
    }
  }
};
