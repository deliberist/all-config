'use strict';

const fs = require('fs');
const util = require('util');

/** A promisfied version of {@code fs.readFile()}. */
fs.readFileAsync = util.promisify(fs.readFile);

/** A mapping of supported file type to a parser for that file type. */
const parsers = Object.freeze({
    'ini': require('./parsers/ini'),
    'json': require('./parsers/json'),
    'properties': require('./parsers/properties'),
    'yaml': require('./parsers/yaml'),
});

/**
 * This method extracts the file extension from the given file.  By pulling out
 * the file extension we can roughly determine the file type of the file.  And
 * thus easily determine which parser to use to convert the raw config data into
 * a more programmatic config object.
 *
 * @param filename The filename to extract the file extension from.
 * @returns {string} The file extension.
 * @throws {string} If the file extension could not be determined.
 */
function getExtension(filename) {
    const extIndex = filename.lastIndexOf('.');
    if (extIndex < 0) {
        throw `File type cannot be determined: ${filename}`;
    }
    return filename.substr(filename.lastIndexOf('.') + 1);
}

// todo test
/**
 * Interpolates the original config object into a new config object based on the
 * different interpolation options.
 *
 * The current interpolation schemes supported include:
 *  -   env: if true, interpolation variables can come from environment
 *          variables.
 *  -   self: if true, interpolation variables can come from within the current
 *          object.
 *
 * Additionally, there are options to control what happens when various edge
 * cases occur during variable interpolation variable.  Those options include:
 *  -   selfRecurseMax: The maximum number of attempts to recursively
 *          interpolate values.
 *  -   ignoreMissing: if true, missing variables are ignored and the original
 *          value is maintained.
 *  -   logErrors: if true, a message is logged if an interpolation variable
 *          cannot be found.
 *  -   raiseErrors: if true, an exception will be raised if an interpolation
 *          variable cannot be found.
 *
 * @param originalConfig The raw config contents in the form of an object.
 * @param interpolateOpts: The options to config the interpolation process.
 * @returns {*} A new object with values interpolated.
 */
function interpolate(originalConfig, interpolateOpts) {
    const getOpt = (opts, optName, defaultValue) => {
        if (typeof opts === 'undefined' || opts === null) {
            return false;
        } else if (!opts.hasOwnProperty(optName)) {
            return false;
        } else {
            const opt = opts[optName];
            if (typeof opt !== 'undefined' && opt !== null) {
                return opt;
            } else {
                return defaultValue;
            }
        }
    };
    // TODO refactor interpolators so that these options work for all interpolators

    // mode = 'lazy' or 'greedy'
    // const interpMode = getOpt(interpolateOpts, 'mode', 'greedy');
    const interpEnv = getOpt(interpolateOpts, 'env', false);
    const interpSelf = getOpt(interpolateOpts, 'self', false);
    const interpSelfRecurseMax = getOpt(interpolateOpts, 'selfRecurseMax', 100);
    const logErrors = getOpt(interpolateOpts, 'logErrors', true);
    const raiseErrors = getOpt(interpolateOpts, 'raiseErrors', false);

    // todo implement

    return originalConfig;
}

/**
 * Reads the specified config file and returns an object representing the
 * properties.  The type of the file is roughly estimated based on the file
 * extension.  If the extension is an unknown type then an exception will be
 * thrown.
 *
 * Additionally, the interpolateOpts defines the options for interpolation.
 *
 * @param filename The filename to extract a config from.
 * @param interpolateOpts: The options to config the interpolation process.
 * @returns {Promise<*>} A Promise, with a resolution being the config
 *      properties.
 * @see interpolate for interpolation options.
 */
module.exports.read = async (filename, interpolateOpts) => {
    const ext = getExtension(filename);
    if (!parsers.hasOwnProperty(ext)) {
        throw `Unknown file extension: ${ext}`;
    }
    const contents = await fs.readFileAsync(filename);
    const config = parsers[ext].parse(contents.toString());
    return interpolate(config, interpolateOpts);
};
