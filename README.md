# all-config

[![npm version](https://badge.fury.io/js/ya-config.svg)](https://badge.fury.io/js/ya-config)
[![npm](https://img.shields.io/npm/dw/ya-config.svg)](https://www.npmjs.com/package/ya-config)

[![pipeline status](https://gitlab.com/rbprogrammer/all-config/badges/master/pipeline.svg)](https://gitlab.com/rbprogrammer/all-config/commits/master)
[![coverage report](https://gitlab.com/rbprogrammer/all-config/badges/master/coverage.svg)](https://gitlab.com/rbprogrammer/all-config/commits/master)

Yet another config module for NodeJS.  There are already too many, but somehow
none of them seemed to suit my needs.  Nor did any come close where I can make
quick modifications to fit into my requirements.  Thus, _all-config_ was born.

Due to the very nature of all-config supporting multiple config file formats, 
dependencies were chosen carefully as to not needlessly require many nested
dependencies. The goal is to eventually make the project truly dependency-less, 
but the main priority was to add functionality first. Then remove dependencies.
In the mean time, there are no formal dependencies to install all-config, but
there are runtime dependencies depending on which file format you require 
parsing.  This was a design decision so that the minimal number of dependencies
are installed for production use.

## Installation

```bash
# Parse ini files.
npm install --save --production all-config ini@1.3.5

# Parse json files - no extra dependencies needed since Nodejs natively supports
# the JSON format.
npm install --save --production

# Parse properties files.
npm install --save --production all-config properties-reader@0.0.16

# Parse yaml files.
npm install --save --production all-config yaml@1.2.1
```

## Usage

TODO - Since this project is a work in progress usage docs will come once a more
stable public API is developed.

### Node JS

TODO

### CLI

TODO

## Tests

```bash
# Just run the unit tests.
npm run test

# Run the unit tests but generate coverage data.
npm run coverage

# Open the coverage results in the default browser (via xdg-open).
npm run coverage-results
```
