# TODO list

- [ ] Rename thisproject to "any-config"?

## File Types
- [x] Add an initial implementation that reads in a ini file.
- [ ] Add an initial implementation that reads in a Java properties file.
- [x] Add an initial implementation that reads in a json file.
- [x] Add an initial implementation that reads in a yaml file.

## Interpolation
- [ ] Keep interpolation turned off by default, and use constructor args to turn it on.
- [ ] Add the ability to do environment variable interpolation.
- [ ] Add the ability to do variable interpolation within the current file.

## Nesting
- [ ] Add the ability to nest (or include) configs, where child configs override parent configs.

## CLI
- [ ] There should be a CLI command to do all of this as well!!
